#include "string.h"
#include "iot_debug.h"
#include "iot_lcd.h"
#include "iot_camera.h"
#include "iot_pmd.h"
#include "demo_camera.h"
#include "demo_lcd_9341.h"
#include "iot_zbar.h"
#include "iot_uart.h"
#include "demo_zbar.h"
#include "disp.h"

unsigned char g_scanner_buff[CAMERA_PREVIEW_WITHE*CAMERA_PREVIEW_HEIGHT/4];
#define ZBAR_IMAGE_START_X 0
#define ZBAR_IMAGE_START_Y 0
#define ZBAR_IMAGE_END_X (CAMERA_PREVIEW_WITHE/2)
#define ZBAR_IMAGE_END_Y (CAMERA_PREVIEW_HEIGHT/2)

HANDLE g_zbar_handle;
ZBAR_CONTEXT g_zbar_context;

static void zbar_scanner_run(int width, int height, int size, char *dataInput)
{
	int len;
	char *data;
    
	//创建句柄， handle != 0 表示解码成功
	int handle = iot_zbar_scannerOpen(width, height, size, dataInput);
  
	if (handle)
	{
		do
		{ 
			// 解码成功获取二维码信息
			data = iot_zbar_getData(handle, &len);
			data[len] = 0;

			camera_preview_close();
			disp_zbar_test_end(data);
			iot_debug_print("[zbar] zbar_scanner_run come in handle_data %s", data);
			// 判断是否有下一个数据
		}while(iot_zbar_findNextData(handle) > 0);

		// 释放句柄
		iot_zbar_scannerClose(handle);
	}
}

void lcdZbarMsgSend(unsigned char *data)
{
  ZBAR_MESSAGE *msg = NULL;

	if (g_zbar_context.status != ZBAR_SCANNER_STATUS_IDLE)
		return;
  
	msg = (ZBAR_MESSAGE *)iot_os_malloc(sizeof(ZBAR_MESSAGE));

	if (!msg)
		return;
	
	msg->type = ZBAR_TYPE_CAMERA_DATA;
	msg->height = ZBAR_IMAGE_END_Y - ZBAR_IMAGE_START_Y;
	msg->width = ZBAR_IMAGE_END_X - ZBAR_IMAGE_START_X;
	msg->dataLen = (ZBAR_IMAGE_END_Y - ZBAR_IMAGE_START_Y) * (ZBAR_IMAGE_END_X - ZBAR_IMAGE_START_X);
	msg->data = data;

	g_zbar_context.status = ZBAR_SCANNER_STATUS_RUNNING;
    iot_os_send_message(g_zbar_handle, msg);
}

char * camera_yuv_conver_y(unsigned char *data)
{

  // 解析黑白图片， 减少解析的一半图片
  unsigned char *src = data,*end = data + (CAMERA_PREVIEW_WITHE/2*CAMERA_PREVIEW_HEIGHT/2*2);
  unsigned char *dst = g_scanner_buff;
	while (src < end)
	{
		*dst = *src;
		src += 2;
		dst++;
	}//End of while;

  return g_scanner_buff;
}


static void zbar_task_main(PVOID pParameter)
{
    ZBAR_MESSAGE*    msg;
   
    while(1)
    {
        iot_os_wait_message(g_zbar_handle, (PVOID)&msg);

        switch(msg->type)
        {
            case ZBAR_TYPE_CAMERA_DATA:
              
            { 

              if (msg->data)
              {
                zbar_scanner_run(msg->width, msg->height, msg->dataLen, camera_yuv_conver_y(msg->data));
              }

              g_zbar_context.status = ZBAR_SCANNER_STATUS_IDLE;
              break;
            }
            default:
                break;
        }

        if (msg)
        {
    			iot_os_free(msg);
    			msg = NULL;
    		}
    }
}

void zbar_task(void)
{
    iot_debug_print("[zbar] zbar_task");
    
    g_zbar_handle = iot_os_create_task((PTASK_MAIN)zbar_task_main, NULL,  15*1024, 0, OPENAT_OS_CREATE_DEFAULT, "zbar task");
    g_zbar_context.status = ZBAR_SCANNER_STATUS_IDLE;
}
