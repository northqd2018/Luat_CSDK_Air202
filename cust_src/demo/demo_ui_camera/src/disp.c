#include "string.h"
#include "stdio.h"
#include "stdarg.h"
#include "iot_os.h"
#include "iot_lcd.h"
#include "iot_debug.h"
#include "disp.h"
#include "iconv.h"
#include "demo_lcd_9341.h"

#define COLOR_WHITE 0xffff
#define COLOR_BLACK 0x0000
#define MAX_FONTS   10

static FontInfo dispFonts[MAX_FONTS];
static u8 curr_font_id = 0;
static FontInfo *dispHzFont;
static int disp_bkcolor = COLOR_WHITE;
static int disp_color = COLOR_BLACK;
static int lcd_width = 240;
static int lcd_height = 320;
u16 framebuffer[240*320];

static void getFontBitmap(DispBitmap *pBitmap, u16 charcode)
{
    const FontInfo *pInfo = &dispFonts[curr_font_id];

    pBitmap->bpp = 1;
    pBitmap->width = pInfo->width;
    pBitmap->height = pInfo->height;

    if(pInfo->data)
    {
        if(charcode >= pInfo->start && charcode <= pInfo->end)
        {
            u32 index = charcode - pInfo->start;

            pBitmap->data = pInfo->data + index*pInfo->size;            
        }
        else
        {
            pBitmap->data = blankChar;
        }
    }
    else
    {
        pBitmap->data = blankChar;
    }
}

static void getHzBitmap(DispBitmap *pBitmap, u16 charcode)
{
    const FontInfo *pInfo = dispHzFont;

    pBitmap->bpp = 1;
    pBitmap->width = pInfo->width;
    pBitmap->height = pInfo->height;

    if(pInfo->data)
    {
        u8 byte1, byte2;
        u32 index;

        byte1 = charcode>>8;
        byte2 = charcode&0x00ff;

        if(byte1 >= 0xB0 && byte1 <= 0xF7 &&
            byte2 >= 0xA1 && byte2 <= 0xFE)
        {
            index = (byte1 - 0xB0)*(0xFE - 0xA1 + 1) + byte2 - 0xA1;
            pBitmap->data = pInfo->data + index*pInfo->size;
        }
        else
        {
            pBitmap->data = blankChar;

        /*+\NEW\liweiqiang\2013.12.18\增加中文标点符号的显示支持 */
            for(index = 0; index < sizeof(sansHzFont16ExtOffset)/sizeof(u16); index++)
            {
                if(charcode < sansHzFont16ExtOffset[index])
                {
                    break;
                }

                if(charcode == sansHzFont16ExtOffset[index])
                {
                    pBitmap->data = sansHzFont16ExtData + index*pInfo->size;
                    break;
                }
            }
        /*-\NEW\liweiqiang\2013.12.18\增加中文标点符号的显示支持 */
        }
    }
    else
    {
        pBitmap->data = blankChar;
    }
}

/*+\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
#if defined(FONT_ESPANA_SUPPORT)
static void getEsFontBitmap(DispBitmap *pBitmap, u16 charcode)
{
    const FontInfo *pInfo = &sansHzFontEs;

    pBitmap->bpp = 1;
    pBitmap->width = pInfo->width;
    pBitmap->height = pInfo->height;
    u32 index;

    if(pInfo->data)
    {
        pBitmap->data = blankChar;
        
        for(index = 0; index < sizeof(EsFont16Offset)/sizeof(u16); index++)
        {
            if(charcode < EsFont16Offset[index])
            {
                break;
            }

            if(charcode == EsFont16Offset[index])
            {
                pBitmap->data = EsFont16Data + index*pInfo->size;
                break;
            }
        }
    }
    else
    {
        pBitmap->data = blankChar;
    }
}
#endif
/*-\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */

static void getCharBitmap(DispBitmap *pBitmap, u16 charcode)
{
    if(charcode >= 0xA0A0)
    {
       /*+\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
       #if defined(FONT_ESPANA_SUPPORT)
        if(charcode >= 0xa8a1 && charcode <= 0xa8bf)    
	    {
            getEsFontBitmap(pBitmap, charcode);
        }
        else
       #endif
       /*-\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
        {
            getHzBitmap(pBitmap, charcode);
        }
    }
    else
    {
        /*+\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
        #if defined(FONT_ESPANA_SUPPORT)
        if(charcode > 0x7e)
        {
            getEsFontBitmap(pBitmap, charcode); 
        }
        else
        #endif
        /*-\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
        {
             getFontBitmap(pBitmap, charcode); 
        }
    }
}

static void disp_1bitbmp_bpp16(const DispBitmap *pBitmap, u16 startX, u16 startY)
{
    u16 bx,by,x,y,bwbytes;
    u16 endX, endY;
    u16 *buffer16 = (u16*)framebuffer;
    u16 temp;

    temp = (disp_color >> 8 ) | ((disp_color & 0xff) << 8);

    endX = MIN(lcd_width,startX + pBitmap->width);
    endY = MIN(lcd_height,startY + pBitmap->height);
    
    bwbytes = (pBitmap->width+7)/8;

    for(x = startX,bx = 0; x < endX; x++,bx++)
    {
        for(y = startY,by = 0; y < endY; y++,by++)
        {            
            if(pBitmap->data[bwbytes*by+bx/8]&(0x80>>(bx%8)))
            {
                //填充颜色
                buffer16[y*lcd_width + x] = temp;
            }
            else
            {
                //填充背景色
                // 直接叠加显示,暂不支持背景色设置
                //buffer16[y*lcd_width + x] = disp_bkcolor;
            }
        }
    }
}

iconv_t disp_conv_open(const char *tocode, const char *fromcode) 
{
    return iconv_open(tocode, fromcode);
} 

BOOL disp_conver(iconv_t cd, size_t ibleft, char *inbuf, size_t *obleft, char **oBuf)
{
    size_t obsize = (ibleft > 256) ? ibleft : 256; 
    size_t ret = -1;

    *obleft = obsize;
    *oBuf = (char*) iot_os_malloc(obsize * sizeof(char));
    if (*oBuf == NULL) {
        return FALSE;
    }

    ret = iconv(cd, &inbuf, &ibleft, oBuf, obleft);
   
    return TRUE;
}

static int disp_conv_close(iconv_t cd) 
{
  iconv_close(cd);

  return 0;
}

void disp_clear(void)
{
  int i,j;

  for (i=0; i<lcd_height;i++)
    {
      for (j=0; j<lcd_width;j++)
      {
        framebuffer[i*lcd_width + j] = COLOR_WHITE;
      }
    }
  
}

void disp_puttext(const char *string, u16 x, u16 y)
{
    int len = strlen(string);
    const u8 *pText = (const u8 *)string;
    int i;
    DispBitmap bitmap;
    u16 charcode;

    if(x >= lcd_width)
        x = 0;

    if(y >= lcd_height)
        y = 0;


    for(i = 0; i < len; )
    {
        if(pText[i] == '\r' || pText[i] == '\n'){
            i++;
            goto next_line;
        }

        
        if(pText[i]&0x80)
        {
            if(pText[i+1]&0x80)
            {
                // gb2312 chinese char
                charcode = pText[i]<<8 | pText[i+1];
                i += 2;
            }
            /*+\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
            #if defined(FONT_ESPANA_SUPPORT)
            else if(pText[i+1] == 0x01)
            {
                charcode = pText[i];
                i += 2;
            }
            #endif
            /*-\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
            else
            {
                charcode = '?';
                i += 1;
            }
        }
        else
        {
            // ascii char
            charcode = pText[i];
            i += 1;
        }


        getCharBitmap(&bitmap, charcode);

        
        disp_1bitbmp_bpp16(&bitmap, x, y);

next_line:   
        // 自动换行显示
        if(x + bitmap.width >= lcd_width)
        {
            y += bitmap.height;
            x = 0;
        }
        else
        {
            x += bitmap.width;
        }

        //自动回显
        if( y >= lcd_height)
        {
            y = 0;
        }
    }

    {
      T_AMOPENAT_LCD_RECT_T rect;
      
      rect.ltX = 0;
      rect.ltY = 0;
      rect.rbX = lcd_width-1;
      rect.rbY = lcd_height-1;

      lcdSetWindowAddress(&rect);
      iot_lcd_update_color_screen(&rect, framebuffer);
    }
}

static void fontInit(void)
{
    static BOOL inited = FALSE;

    if(inited)
        return;

    inited = TRUE;
#if defined(LUA_DISP_SUPPORT_HZ)
    sansHzFont16.data = sansHzFont16Data;
#else
    sansHzFont16.data = NULL;
#endif

/*+\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
#if defined(FONT_ESPANA_SUPPORT)
    sansHzFontEs.data = EsFont16Data;
#endif
/*-\NEW\shenyuanyuan\2017.12.5\增加西班牙特殊字符的显示支持 */
    
    memset(dispFonts, 0, sizeof(dispFonts));

    dispFonts[0] = sansFont16;
    dispHzFont = &sansHzFont16;
}

int disp_clear_x(int x, int y, int len)
{
  int i,j;
  int y_end = (y+sansHzFont16.height<lcd_height-1)? (y+sansHzFont16.height):(lcd_height-1);
  int x_end = (x+(sansHzFont16.width*len)<lcd_width-1)? (x+(sansHzFont16.width*len)):(lcd_width-1);

  for (j=y; j<=y_end; j++)
  {
    for (i=x; i<=x_end; i++)
    {
      framebuffer[j*lcd_width+i] = COLOR_WHITE;
    }
  }
}

int disp_centere_x(char *data)
{
  int centere = 0;
  int dataLen = strlen(data);

  if (dataLen*sansHzFont16.width/2 > lcd_width-1)
  {
    return 0;
  }

  centere = (lcd_width - (dataLen*sansHzFont16.width/2))/2;

  iot_debug_print("disp_centere_x %d,%d,%d", centere, sansHzFont16.width, dataLen);
  return centere;
}

void disp_keypad_test_end(BOOL end)
{
  if (end)
  {
    disp_clear();
    disp_puttext("键盘测试", disp_centere_x("扫码测试"), 10);
    disp_puttext("测试结束", disp_centere_x("测试结束"), 50);
    disp_puttext("PASS", disp_centere_x("PASS"), 80);
    disp_puttext("按[A] 退出测试", 0, 260);
    disp_puttext("按[B] 继续测试", 0, 260+sansHzFont16.height);
  }
  else
  {
    disp_clear();
    disp_puttext("键盘测试", disp_centere_x("键盘测试"), 10);
    disp_puttext("测试结束", disp_centere_x("测试结束"), 50);
    disp_puttext("FAIL", disp_centere_x("FAIL"), 80);
    disp_puttext("按[A] 退出测试", 0, 260);
    disp_puttext("按[B] 继续测试", 0, 260+sansHzFont16.height);
  }
}

void disp_keypad_test_process(char *count)
{

  disp_puttext("已按键数:", disp_centere_x("已按键数:"), 50+sansHzFont16.height*4);
  disp_clear_x(disp_centere_x(count),50+sansHzFont16.height*5,2);
  disp_puttext(count, disp_centere_x(count), 50+sansHzFont16.height*5);
  disp_puttext("PASS", disp_centere_x("PASS"), 50+sansHzFont16.height*6);
}

void disp_keypad_test_begin(void)
{
  disp_clear();
  disp_puttext("键盘测试", disp_centere_x("键盘测试"), 10);
  disp_puttext("请依次输入按键", disp_centere_x("请依次输入按键"), 50);
  disp_puttext("1,2,3,4,5,6,7,8,9", disp_centere_x("1,2,3,4,5,6,7,8,9"), 50+sansHzFont16.height);
  disp_puttext("A,B,C,D", disp_centere_x("A,B,C,D"), 50+sansHzFont16.height*2);
  disp_puttext("*,0,#", disp_centere_x("*,0,#"), 50+sansHzFont16.height*3);
}

void disp_zbar_test_end(char *data)
{
  disp_clear();
  disp_puttext("扫码测试", disp_centere_x("扫码测试"), 10);
  disp_puttext("扫码成功", 0, 50);
  disp_puttext(data, 0, 50+sansHzFont16.height);

  disp_puttext("按[A] 退出测试", 0, 260);
  disp_puttext("按[B] 继续测试", 0, 260+sansHzFont16.height);
}

void disp_zbar_test_begin(void)
{
  disp_clear();
  disp_puttext("扫码测试", disp_centere_x("扫码测试"), 10);
  disp_puttext("扫描一维码//扫码二维码", 0, 100);
}

void disp_idle(void)
{
  disp_clear();
  disp_puttext("www.openluat.com", disp_centere_x("www.openluat.com"), 10);

  disp_puttext("1.键盘测试", 0, 80);
  disp_puttext("2.扫码测试", 0, 110);
  disp_puttext("3.预览测试 <任意键退出预览>", 0, 140);
}

void disp_init(void)
{
  fontInit();
  disp_idle();
}