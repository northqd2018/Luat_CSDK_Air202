#ifndef __DEMO_ZBAR_H__
#define __DEMO_ZBAR_H__

#include "iot_os.h"

typedef enum
{
  APP_STATUS_NULL, 
  APP_STATUS_IDLE,
  APP_STATUS_KEY,
  APP_STATUS_ZBAR,
  APP_STATUS_PREVIEW,
  APP_STATUS_SELECT,
} APP_STATUS;

typedef enum
{
    ZBAR_TYPE_CAMERA_DATA,
    ZBAR_TYPE_CAMERA_DATA_END,
}ZBAR_MESSAGE_TYPE;

typedef enum
{
    ZBAR_SCANNER_STATUS_NULL,
    ZBAR_SCANNER_STATUS_IDLE,
    ZBAR_SCANNER_STATUS_START,
    ZBAR_SCANNER_STATUS_RUNNING,
    ZBAR_SCANNER_STATUS_STOP
}ZBAR_SCANNER_STATUS;

typedef struct {
    unsigned char *data;
    int dataLen;
    int width;
    int height;
    ZBAR_MESSAGE_TYPE type;
}ZBAR_MESSAGE;


typedef struct
{
    ZBAR_SCANNER_STATUS status;
}ZBAR_CONTEXT;

extern void zbar_task(void);
void lcdZbarMsgSend(unsigned char *data);
#endif
