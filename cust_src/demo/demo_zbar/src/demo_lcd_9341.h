#ifndef __DEMO_LCD_CONFIG_H__
#define __DEMO_LCD_CONFIG_H__

#define LCD_9341

#define LCD_I2C_ADDR (0X21)
#define LCD_PREVIEW_END_X (240)
#define LCD_PREVIEW_END_Y (320)
#define CAMERA_PREVIEW_WITHE (240)
#define CAMERA_PREVIEW_HEIGHT (320)
extern void lcd_open(void);
#endif
